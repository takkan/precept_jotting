# Precept Documentation


## Build

Uses VuePress

To build once:

``` bash

yarn build

```


To build and monitor changes to source

``` bash
yarn dev
```