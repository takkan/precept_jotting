---
home: true
heroImage: small-logo.png
tagline: Application Framework for Flutter with Remote Updates
---

## Key Features

- Configuration of Widget selection and layout by declarative JSON file.  

- Configuration of schema by declarative JSON file, ensuring data and presentation stay in sync

- Load configuration from server and ensure that clients are updated without any action from users

- Automatic data binding from the above declarations, making Forms really simple

- Built in edit / save / cancel functionality for Forms, with validation and permissions provided by the schema

- Could be used with any backend

::: danger

Please be aware that although this documentation is written as though functionality already exists, that may not always be the case.

Sometimes the documentation is ahead of development.

That will no longer be the case once the package is published.
:::
----


# Contents


| Guides                                       | Project                                                       |
|----------------------------------------------|---------------------------------------------------------------|
| [Tutorial](./tutorial/README.md)             | [Project Status](./status.md)                                 |
| [User Guide](./user-guide/README.md)         | [Issues](https://gitlab.com/precept1/precept-client/-/issues) |
| [Developer Guide](developer-guide/README.md) | [Code](https://gitlab.com/precept1/precept-client)            |

